import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import rich
from rich.status import Status

with rich.status.Status(f"Opening Firefox..."):
    firefox = webdriver.Firefox()
with rich.status.Status(f"Opening Printables.com..."):
    firefox.get("https://printables.com")
with rich.status.Status(f"Waiting for Cookie button..."):
    try:
        firefox.find_element(By.ID, "onetrust-accept-btn-handler").click()
    except NoSuchElementException:
        time.sleep(1)
with rich.status.Status(f"Waiting Login button..."):
    try:
        firefox.find_element(By.XPATH, '//button[contains(text(),"Login")]').click()
    except NoSuchElementException:
        time.sleep(1)
